import App from './App.svelte';
// import './main.css';// already imported in index.html

// PWA Stuff
/* eslint-disable func-names */
;(function() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('./service-worker.js', { scope: '/' })
      .then(() => console.log('Service Worker registered successfully.'))
      .catch((error) => console.log('Service Worker registration failed:', error))
  }
})()
 
const app = new App({
  target: document.body,
  // props: {
  //   name: 'world',
  // },
});

export default app;
