import { writable } from 'svelte/store';
import { log } from '../config/settings'; // ,online

const defupdate = {
  refreshed: 0,
  success: false,
  // online: true,
  counter: 0,
  current: null,
};

export const updateStore = writable(defupdate, async function start(set) {
  // console.log(getResult, defupdate)
  // console.log('newUpdate', newUpdate)

  set(defupdate);
  // console.log($updateStore);
});
// export const updateStore = writable(defupdate);
