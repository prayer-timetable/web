// @ts-ignore
import localforage from 'localforage';

import { settingsStore } from '../settingsStore';
import { log } from '../../config/settings'; // ,online

// import { settings } from '../settings'

/**
 * **********
 * SET
 * **********
 */

const toggleSet = async (jamaahShow) => {

  // console.log('toggle in store',toggle)
  settingsStore.update(n => {
      n.jamaahShow = jamaahShow;
      return n;
    });

    // console.log(result)

    await localforage.setItem('jamaahShow', jamaahShow);

  return jamaahShow;
};

/**
 * **********
 * GET
 * **********
 */
const toggleGet = async () => {
  let result = null;

  try {
    result = await localforage.getItem('jamaahShow');

    log && console.log('got storred jamaahShow', await result);
  } catch (error) {
    /* eslint-disable prefer-destructuring */
    log && console.log('failed geting local storage', error);
  }
  // logging && console.log('got api settings', newsettings)

  return result;
};


/**
 * **********
 * EXPORTS
 * **********
 */

// export default update
export { toggleSet, toggleGet };
