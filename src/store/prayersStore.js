import { readable } from 'svelte/store';

import { prayersCalc } from 'prayer-timetable-lib'; // ,dayCalc
// import { prayersCalc } from '../helpers/prayerTimetableCalc'; // ,dayCalc
import timetable from '../config/timetable';
// import defsettings from '../config/settings';
import { settingsStore } from './settingsStore';
import { updateStore } from './updateStore';

let settings;
const unsubscribeSettings = settingsStore.subscribe(value => {
  // console.log(value);
  settings = value;
});

if (settings.join === '0') settings.join = false;

const prayersGet = prayersCalc(timetable, settings, settings.jamaahShow);

// first argument for readable is default - initial value
export const prayersStore = readable(prayersGet, async function start(set) {


// INTERVAL
  const interval = setInterval(() => {
    const calc = prayersCalc(timetable, settings, settings.jamaahShow);

    set(calc);

  }, 1000);

  return function stop() {
    clearInterval(interval);
    unsubscribeSettings();
  };
});
