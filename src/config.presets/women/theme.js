// ***************** */
//  BASE STYLE       */
// ***************** */
const mainColor = '#333';
const backgroundColor = '#aeaecf'; // choose null if non-transparent background image should be used

const appStyle = `${backgroundColor ? `background-color:${backgroundColor};` : ''}color:${mainColor};fill:${mainColor}`;

// ***************** */
//  OVERRIDE STYLES  */
// ***************** */
const headerStyle = 'background: none; background-image: linear-gradient(rgba(0,0,0,.5), rgba(0,0,0,.15), rgba(0,0,0,0));color: #f9f9f9;';
const logoStyle = 'fill: #f9f9f9;';
const clockStyle = 'align-items: flex-end;margin-top: 2vh;border: none;';

const prayersStyle = 'border-top:none;';
const prayerRowStyle = '';
// const prayerRowStyle = 'background: rgba(0, 0, 55, 0.3);'
const prayerHeaderStyle = '';
const prayerNextStyle = '';

const messageStyle = '';
const footerStyle = '';

export default appStyle;
export { appStyle, headerStyle, logoStyle, clockStyle, prayersStyle, prayerRowStyle, prayerHeaderStyle, prayerNextStyle, messageStyle, footerStyle };
