// ***************** */
//  BASE STYLE       */
// ***************** */
const mainColor = '#fff';
const backgroundColor = '#011289'; // choose null if non-transparent background image should be used
const highlightColor = '#ee3300';

const appStyle = `background-color:${backgroundColor};color:${mainColor};fill:${mainColor}`;

// ***************** */
//  OVERRIDE STYLES  */
// ***************** */

//* **  HEADER **/
const headerStyle = `background: ${highlightColor}`;
const logoStyle = 'width:10vh';

//* **  CLOCK **/
const clockStyle = ``;

//* **  PRAYERS **/
const prayersStyle = `padding: 0;background:${backgroundColor};border-top: 0.25vh solid ${highlightColor};border-bottom: 0.25vh solid ${highlightColor};`;
const prayerHeaderStyle = `color:${highlightColor};padding: 1.5vh 2vh;margin: 0;font-size:100%;background:${backgroundColor};`;
const prayerMainStyle = ``;
const prayerRowStyle = ''//`background:${backgroundColor};margin:1px 0 0;`;
const prayerNextStyle = `background-color:${highlightColor};color: ${mainColor}`;

//* **  MESSAGE **/
const messageStyle = '';
const messageTextStyle = 'font-size:80%;font-weight:700;';
const messageRefStyle = 'font-size:75%;';

//* **  COUNTDOWN **/
const countdownStyle = `text-transform:uppercase;font-size: 100%;line-height:1.15;border-bottom: .25vh solid ${backgroundColor}`;
const barStyle = `background: ${highlightColor}`;
const barBgStyle = `background: ${mainColor}`;

//* **  FOOTER **/
const footerStyle = `background: ${highlightColor}`;

export default appStyle;
export {
  mainColor,
  backgroundColor,
  highlightColor,
  appStyle,
  headerStyle,
  logoStyle,
  clockStyle,
  prayersStyle,
  prayerRowStyle,
  prayerHeaderStyle,
  prayerMainStyle,
  prayerNextStyle,
  messageStyle,
  messageTextStyle,
  messageRefStyle,
  countdownStyle,
  barStyle,
  barBgStyle,
  footerStyle,
};
