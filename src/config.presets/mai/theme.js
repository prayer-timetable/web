// ***************** */
//  BASE STYLE       */
// ***************** */
const mainColor = '#333';
const backgroundColor = '#ca6'; // choose null if non-transparent background image should be used

const appStyle = `${backgroundColor ? `background-color:${backgroundColor};` : ''}color:${mainColor};fill:${mainColor}`;

// ***************** */
//  OVERRIDE STYLES  */
// ***************** */
const headerStyle = '';
const logoStyle = '';
const clockStyle = '';

const prayersStyle = '';
const prayerRowStyle = '';
const prayerHeaderStyle = '';
const prayerNextStyle = '';

const messageStyle = '';
const footerStyle = '';

export default appStyle;
export { appStyle, headerStyle, logoStyle, clockStyle, prayersStyle, prayerRowStyle, prayerHeaderStyle, prayerNextStyle, messageStyle, footerStyle };
