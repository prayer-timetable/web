# Prayer timetable

Muslim prayer timetable app for deployment on Raspberry Pi. It is meant to be used on public displays to show your local community's prayer times. Can be used in masjids, mosques or even at home. Optimised for Full HD screens. Made using Svelte, with date-fns for time manipulation. Please note that this is in ~~alpha~~ beta stage and built specifically for my own needs.

This software is provided as-is, with no warranty of any kind. I provide it for free and do not expect and/or ask for any compensation. What's more, I will help you setup your own presentation screen and even install for free, but you will cover the postal bills in case you send me your own Raspberry Pi for setup.

## Yarn

Although you can use npm command for all the steps, I recommend using Yarn package. It significantly speeds up the compilation process on Rasperry Pi.

    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo apt-get update && sudo apt-get install yarn

## Quick Start

### Standalone app:

First clone the repo:

    git clone https://gitlab.com/ench0/prayer-timetable-svelte

    cd prayer-timetable-svelte

Second, get the packages:

    yarn

or

    npm install

Bootstrap (check Settings file section below):

    yarn setup:default

To start the app in dev mode, use:

    yarn dev

or

    npm run start

Aceess the app via http://localhost:5000/

To build static files (html + css + image) use:

    yarn build

or

    npm run build

Files are placed in /dist folder.

### As module (WIP):

    import timetable from 'prayer-timetable-svelte'

## Settings files

There are 5 files that need to be placed in 'src/config' folder, used to define all aspects of the app.

To use default settings (offline app, controled through settings.json file), while inside the prayer-timetable-svelte folder (check above for info on cloning) run

    yarn setup:default

or if you are on Powershell:

    yarn setup:default:win

That will create 5 files needed for the operation. Check and change them as needed.

The files are:

** settings.json **: settings file

** timetable.json **: timetable file

** theme.js **: theme file

** logo.svg **: logo file

** bg.png **: background image file

These are the options available in aforementioned settings file:

**title**: obviously title of your organisation.

**lang**: not implemented yet.

**online**: if you do not want to fetch the contents from your own API but rather do it through the config file, leave this at false.

**url**: {
**settings**: API link to your settings
**timetable**: if your timetable keeps changing often, use this for the API of your timetable

**announcement**: message shown as a title in bottom message area

**text**:
**en**: English text for message area.
**ar**: Arabic text for message area.

**hijrioffset**: if you need to adjust your Hijri date by +- 1 or 2 days, use this option.

**jummuahtime**: time for Jummuah prayer.

**taraweehtime**: time of Taraweeh prayer.

**jamaahmethods**: 6 methods of calculation, can be: fixed (fixed prayer time), afterthis (hr/mins after the adhan), beforenext (hr/mins before next adhan), joined (hr/mins after previous adhan).

**jamaahoffsets**: hrs/mins to match the above methods.

**labels**: not fully implemented yet.

    jummuah, taraweeh, prayer, adhan, iqamah, prepare, ramadancountdown, names: [Fajr Shurooq Dhuhr Asr Maghrib Isha]

**updated**: automatic date in Unix format picked from your own API; date of this settings update.

**join**: optional boolean value to decide whether maghrib and isha prayers should be joined/prayed together.

## Timetable File

Typically, you will have your timetable file in json format and import/conver to object such as this:

    import timetable from './timetable.json'

It is structured as follows:

    {
        "1": {
            "1": [[6,43],[8,37],[12,30],[14,3],[16,19],[18,7]],
            "2": [[6,43],[8,37],[12,31],[14,3],[16,20],[18,8]],
            "3": [[6,43],[8,37],[12,31],[14,4],[16,22],[18,9]],
            ...
        }
    ...
    }

Starts with month (first "1"), followed by day in a month ("1", "2", "3", ...). Every day contains array of six sub-arrays with [hour:minute] (no zero prepend) displayed inside.

Timetable file format used by lib [here](https://gitlab.com/ench0/prayer-timetable-lib/blob/master/README.md). Or use examples from other cities to figure out the JSON format.

## Features

- Prayer times
- Jamaah times
- Jamaah calculation offsets/methods
- Automatic fetch of updates from external API (configurable)
- Next prayer countdown
- Jummuah/Taraweeh notification
- Themes
- Announcements/messages
- Hijri dates
- Hijri offset
- Next day starts after isha

## How can you help

The timetable app currently contains timetables for Dublin. If you like your city/language to be included, please contact me and send your own timetable. You are responsible for accuracy of the data.

## Tools and libraries used

- Svelte JS
- Rollup
- date-fns

## Quick Raspberry Pi Setup (WIP)

First, get latest version of node:

    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

    sudo apt-get install -y nodejs

Optional:

    sudo apt-get install -y build-essential

Second, create autostart link:

    mkdir ~/.config/autostart

    nano ~/.config/autostart/autoChromium.desktop

Paste the following:

    [Desktop Entry]
    Type=Application
    Exec=/usr/bin/chromium-browser --noerrdialogs --disable-session-crashed-bubble --disable-infobars --incognito --kiosk --check-for-update-interval=31556952 --simulate-critical-update file:///home/pi/prayer-timetable-svelte/dist/index.html
    Hidden=false
    X-GNOME-Autostart-enabled=true
    Name[en_US]=AutoChromium
    Name=AutoChromium
    Comment=Start Chromium when GNOME starts

Press ctrl + x to exit the editor and choose 'y' to save.
Third, edit boot config to enable vertical display:

    sudo nano /boot/config.txt

Add this line to the end of the file:

    display_rotate=3

Press ctrl + x to exit the editor and choose 'y' to save.
Fourth, edit this file to disable the screensaver:

    sudo nano /etc/xdg/lxsession/LXDE-pi/autostart

This should be pasted in (and other content removed by ctrl < k >):

    @lxpanel --profile LXDE-pi
    @pcmanfm --desktop --profile LXDE-pi
    @point-rpi
    @xset s noblank
    @xset s off
    @xset -dpms
    @unclutter -display :0 -idle 3 -root -noevents

Press ctrl + x to exit the editor and choose 'y' to save.
Last line above removes the mouse cursor when idle. Install unclutter for that purpose:

    sudo apt install unclutter

Optionally, remove the mouse cursor completely (warning! invisible cursor):

    sudo nano /etc/lightdm/lightdm.conf

    [SeatDefaults]
    #type=xlocal
    #xdg-seat=seat0
    #pam-service=lightdm
    #pam-autologin-service=lightdm-autologin
    #pam-greeter-service=lightdm-greeter
    #xserver-command=X -s dpms
    xserver-command=X -s dpms -nocursor
    #xserver-layout=
    ...
    autologin-user=pi

Lastly, follow the quickstart from the beginning of this file.

## Screenshots

|      MAI theme      |      ICCI Women theme      |
| :-----------------: | :------------------------: |
|  ![MAI theme][mai]  | ![ICCI Women theme][women] |
|     ICCI theme      |         IFI theme          |
| ![ICCI theme][icci] |     ![IFI theme][ifi]      |

[mai]: https://gitlab.com/ench0/prayer-timetable-svelte/raw/master/src/assets/screenshots/mai.png 'MAI theme'
[women]: https://gitlab.com/ench0/prayer-timetable-svelte/raw/master/src/assets/screenshots/women.png 'ICCI Women theme'
[icci]: https://gitlab.com/ench0/prayer-timetable-svelte/raw/master/src/assets/screenshots/icci.png 'ICCI theme'
[ifi]: https://gitlab.com/ench0/prayer-timetable-svelte/raw/master/src/assets/screenshots/ifi.png 'IFI theme'

```

yarn global add install-peerdeps
install-peerdeps --dev eslint-config-airbnb-base
```
