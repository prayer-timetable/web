// const tailwindcss = require('tailwindcss')
const cssnano = require('cssnano');
const purgecss = require('@fullhuman/postcss-purgecss')({
  content: ['./src/**/*.svelte', './src/**/*.html', './src/**/*.svg', './src/**/*.js', './src/**/*.webmanifest'],
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
});
const postcssFontPath = require('postcss-fontpath')({
  checkFiles: true,
  ie8Fix: true,
});
const postcssPresetEnv = require('postcss-preset-env')({ stage: 1 }); // nesting and auto-prefixing

module.exports = {
  plugins: [
    postcssPresetEnv,
    // tailwindcss('./tailwind.config.js'),

    ...(process.env.NODE_ENV === 'production' ? [purgecss, cssnano, postcssFontPath] : []),
  ],
};
